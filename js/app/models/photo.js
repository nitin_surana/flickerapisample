/**
 * Created by Nitin on 9/20/2015.
 */
define(['backbone'
], function (Backbone) {
    "use strict";
    return Backbone.Model.extend({
        initialize: function () {
            this.on('img', this.setImgSrc, this);
        },
        setImgSrc: function () {
            var src = 'https://farm' + this.get('farm') + '.staticflickr.com/' + this.get('server') + '/' + this.get('id') + '_' + this.get('secret') + '.jpg';
            this.set('src', src);
        }
    });
});