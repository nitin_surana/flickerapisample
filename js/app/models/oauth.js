/**
 * Created by Nitin on 9/20/2015.
 */
define(['jquery',
    'backbone',
    'oauth-signature'
], function ($, Backbone) {
    'use strict';

    var model = Backbone.Model.extend({
        defaults: {
            httpMethod: 'GET',
            tokenUrl: 'https://www.flickr.com/services/oauth/request_token',
            parameters: {
                oauth_consumer_key: '6e4d0c1a47190fe1810f405072bf99f4',
                oauth_nonce: parseInt(Math.random() * 100000),//'95613465',
                oauth_timestamp: parseInt((new Date()).getTime() / 1000),
                oauth_signature_method: 'HMAC-SHA1',
                oauth_version: '1.0',
                oauth_callback: 'http://localhost:63342/FlickerApiSample/index.html'
            },
            consumerSecret: 'eb6a0c44c07b4647'
        },
        initialize: function () {
            var self = this,
                encodedSignature = oauthSignature.generate(this.get('httpMethod'), this.get('tokenUrl'), this.get('parameters'), this.get('consumerSecret'));
            this.set('encodedSignature', encodedSignature);

            var requestUrl = 'https://www.flickr.com/services/oauth/request_token' +
                '?oauth_nonce=' + this.get('parameters').oauth_nonce +
                '&oauth_timestamp=' + this.get('parameters').oauth_timestamp +
                '&oauth_consumer_key=' + this.get('parameters').oauth_consumer_key +
                '&oauth_signature_method=' + this.get('parameters').oauth_signature_method +
                '&oauth_version=' + this.get('parameters').oauth_version +
                '&oauth_signature=' + encodedSignature +
                '&oauth_callback=' + this.get('parameters').oauth_callback;

            $.ajax({
                url: 'http://cors-anywhere.herokuapp.com/' + requestUrl
            }).done(function (response) {
                console.log(response);
                var r = new RegExp(/oauth_token=(.*?)&oauth_token_secret=(.+)/);
                var arr = r.exec(response);
                self.set('oauth_token',arr[1]);
                self.set('oauth_token_secret',arr[2]);
                self.trigger('authenticated');
            });
        }
    });
    return model;
});