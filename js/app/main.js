/**
 * Created by Nitin on 9/20/2015.
 */
requirejs.config({
    baseUrl: 'js',
    paths: {
        'jquery': 'jquery',
        'underscore': 'underscore',
        'backbone': 'backbone',
        'bootstrap': 'bootstrap',
        'oauth-signature': 'oauth-signature',
        'select2':'select2'
    },
    shim: {
        'backbone': {
            exports: 'Backbone',
            deps: ['jquery', 'underscore']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'select2': {
            deps: ['jquery']
        }
    }
});

require(['app/shell'], function (ShellView) {
    var shellView = new ShellView({
        el: '.container-fluid'
    });
});
