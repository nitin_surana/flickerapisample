/**
 * Created by Nitin on 9/20/2015.
 */


var httpMethod = 'GET',
    tokenUrl = 'https://www.flickr.com/services/oauth/request_token',
    parameters = {
        oauth_consumer_key: '6e4d0c1a47190fe1810f405072bf99f4',
        //oauth_token : 'nnch734d00sl2jdk',
        oauth_nonce: '95613465',
        oauth_timestamp: '1305586162',
        oauth_signature_method: 'HMAC-SHA1',
        oauth_version: '1.0',
        oauth_callback: 'http://localhost:63342/FlickerApiSample/index.html'
    },
    consumerSecret = 'eb6a0c44c07b4647',
//tokenSecret = 'pfkkdhi9sl3r4s00',

// generates a RFC 3986 encoded, BASE64 encoded HMAC-SHA1 hash
    encodedSignature = oauthSignature.generate(httpMethod, tokenUrl, parameters, consumerSecret),
// generates a BASE64 encode HMAC-SHA1 hash
    signature = oauthSignature.generate(httpMethod, tokenUrl, parameters, consumerSecret,
        {encodeSignature: false});

var requestUrl = 'https://www.flickr.com/services/oauth/request_token' +
    '?oauth_nonce=' + parameters.oauth_nonce +
    '&oauth_timestamp=' + parameters.oauth_timestamp +
    '&oauth_consumer_key=' + parameters.oauth_consumer_key +
    '&oauth_signature_method=' + parameters.oauth_signature_method +
    '&oauth_version=' + parameters.oauth_version +
    '&oauth_signature=' + encodedSignature +
    '&oauth_callback=' + parameters.oauth_callback;

//var vv = window.open(requestUrl);

$.ajax({
    url:'http://cors-anywhere.herokuapp.com/'+requestUrl
}).done(function(response){
    console.log(response);
});