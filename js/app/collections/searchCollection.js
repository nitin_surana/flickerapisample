/**
 * Created by Nitin on 9/20/2015.
 */
define(['backbone',
    'app/models/photo'
], function (Backbone, PhotoModel) {
    "use strict";

    var collection = Backbone.Collection.extend({
        model: PhotoModel,
        url: function () {
            return 'https://api.flickr.com/services/rest/?media=' + this.media + '&sort=' + this.sort + '&page=' + this.page + '&per_page=' + this.per_page + '&method=flickr.photos.search&api_key=a06870e615de68d0b8b675385f2c228f&format=json&nojsoncallback=1&text=' + encodeURIComponent(this.searchTerm);
        },
        parse: function (response) {
            this.totalPages = response.photos.pages;
            return response.photos.photo;
        },
        initialize: function () {
            this.page = 1;
            this.per_page = 12;
            this.sort = 'relevance';
            this.media = 'all';
            this.on('sync', this.triggerModelSync, this);
        },
        triggerModelSync: function () {
            _.each(this.models, function (model) {
                model.trigger('img');
            });
        }
    });
    return collection;
});