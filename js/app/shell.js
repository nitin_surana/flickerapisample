/**
 * Created by Nitin on 9/20/2015.
 */
define([
    'underscore',
    'backbone',
    'text!app/tpl/shell.html',
    'text!app/tpl/searchResult.html',
    'text!app/tpl/loaderTemplate.html',
    'app/collections/searchCollection',
    'app/slider',
    'bootstrap',
    'select2'
], function (_, Backbone, ShellTemplate, SearchResultTemplate, LoaderTemplate, SearchCollection, SliderView) {
    'use strict';

    var view = Backbone.View.extend({
        initialize: function () {
            this.searchCollection = new SearchCollection();
            this.searchCollection.on('sync', this.renderResults, this);
            this.render();
        },
        render: function () {
            this.$el.html(_.template(ShellTemplate)());
            this.$("#sort").select2({
                width: '100%'
            });
            this.$("#media").select2({
                width: '100%'
            });
            return this;
        },
        events: {
            'keyup #search': 'searchApi',
            'mouseover #results img': 'showImgDesc',
            'mouseout #results img': 'hideImgDesc',
            'click #results img': 'showSlider',
            'click #next': 'nextPage',
            'click #previous': 'previousPage',
            'change #sort,#media': 'setFilter'
        },
        setFilter: function (e) {
            this.$("#results").html(_.template(LoaderTemplate)());
            this.searchCollection.sort = this.$('#sort').val();
            this.searchCollection.media = this.$('#media').val();
            this.searchCollection.fetch();
        },
        nextPage: function (e) {
            this.$("#results").html(_.template(LoaderTemplate)());
            this.searchCollection.page++;
            this.searchCollection.fetch();
        },
        previousPage: function (e) {
            this.$("#results").html(_.template(LoaderTemplate)());
            this.searchCollection.page--;
            this.searchCollection.fetch();
        },
        showSlider: function (e) {
            var $target = $(e.currentTarget);
            var index = $target.data('index');
            this.sliderView = new SliderView({
                collection: this.searchCollection,
                index: index
            });
        },
        showImgDesc: function (e) {
            var $target = $(e.currentTarget);
            $target.next(".img-desc").slideDown();
        },
        hideImgDesc: function (e) {
            var $target = $(e.currentTarget);
            $target.next(".img-desc").slideUp();
        },
        searchApi: function (e) {
            if (e.which !== 13) {
                return;
            }
            var searchTerm = this.$("#search").val();
            if (!!searchTerm) {
                this.$("#results").html(_.template(LoaderTemplate)());
                this.searchCollection.searchTerm = searchTerm;
                this.searchCollection.fetch();
            }
        },
        renderResults: function () {
            var self = this;
            this.$("#results").empty();     //remove earlier results
            _.each(this.searchCollection.models, function (model, index) {
                self.$("#results").append(_.template(SearchResultTemplate)({data: _.extend(model.toJSON(), {index: index})}));
            });
            if (this.searchCollection.length) {
                this.$("#results-nav").show();
            } else {
                this.$("#results-nav").hide();
            }
            this.renderNavAndFilter();
        },
        renderNavAndFilter: function () {
            if (this.searchCollection.length) {
                this.$("#results-filter").show();
            } else {
                this.$("#results-filter").hide();
            }
            if (this.searchCollection.page > 1) {
                this.$("#previous").prop('disabled', false);
            } else {
                this.$("#previous").prop('disabled', true);
            }
            if (this.searchCollection.page === this.searchCollection.totalPages) {
                this.$("#next").prop('disabled', true);
            } else {
                this.$("#next").prop('disabled', false);
            }
            this.$(".results-container .current-page").html(this.searchCollection.page);
            this.$(".results-container .total-pages").html(this.searchCollection.totalPages);
        }
    });
    return view;
});