/**
 * Created by Nitin on 9/20/2015.
 */
define(['backbone',
    'text!app/tpl/sliderTemplate.html'
], function (Backbone, SliderTemplate) {
    "use strict";

    return Backbone.View.extend({
        initialize: function (opts) {
            this.collection = opts.collection;
            this.index = opts.index;
            this.render();
        },
        renderImage: function () {
            this.$(".slider-container").html(_.template(SliderTemplate)({data: _.extend(this.collection.at(this.index).toJSON(), {index: this.index})}));
            if (this.index === 0) {
                this.$("#previous").prop('disabled', true);
            } else {
                this.$("#previous").prop('disabled', false);
            }
            if (this.index === this.collection.length - 1) {
                this.$("#next").prop('disabled', true);
            } else {
                this.$("#next").prop('disabled', false);
            }
        },
        render: function () {
            this.$el = $("<div/>", {
                html: "<div class='slider-overlay'></div><div class='slider-container'></div>"
            }).appendTo("body");
            this.delegateEvents();
            this.renderImage();
        },
        events: {
            'click #next': 'nextImage',
            'click #previous': 'prevImage',
            'click .close': 'removeSlider'
        },
        removeSlider: function () {
            this.remove();
        },
        nextImage: function () {
            this.index++;
            this.renderImage();
        },
        prevImage: function () {
            this.index--;
            this.renderImage();
        }
    });
});